﻿namespace Rokolabs.Game.Dto
{
	public class RecordDto
	{
		public string PlayerName { get; set; }

		public long Score { get; set; }

		public RecordDto(string playerName, long score)
		{
			PlayerName = playerName;
			Score = score;
		}

		public RecordDto() { }
	}
}