﻿using System;
using Rokolabs.Game.Dto.Enums;

namespace Rokolabs.Game.Dto
{
	public class GameBaseDto
	{
		public int Damage { get; }

		public int HealthPoints { get; }

		public int BonusStrength { get; }

		public BaseType BaseType { get; }

		public Guid Id { get; set; }

        public GameBaseDto()
        {
        }

        public GameBaseDto(Guid id, string baseType, int? damage, int? healthPoints, int? bonusStrength)
		{
            Id = id;
			Enum.TryParse(baseType, out BaseType type);
			BaseType = type;

			if (damage != null) Damage = damage.Value;
			if (healthPoints != null) HealthPoints = healthPoints.Value;
			if (bonusStrength != null) BonusStrength = bonusStrength.Value;
		}
	}
}