﻿namespace Rokolabs.Game.Dto.Enums
{
	public enum BaseType
	{
		Hero = 1,
		AppleBonus = 2,
		RaspberryBonus = 3,
		BearEnemy = 4,
		WolfEnemy = 5,
		RockHurdle = 6,
		TreeHurdle = 7
	}
}