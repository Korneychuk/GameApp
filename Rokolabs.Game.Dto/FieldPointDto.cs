﻿using System;

namespace Rokolabs.Game.Dto
{
	public class FieldPointDto
	{
		public int I { get; set; }

		public int J { get; set; }

		public Guid? BaseId { get; set; }

		public GameBaseDto GameBaseDto { get; set; }

		public FieldPointDto(int i, int j, Guid? baseId, GameBaseDto baseDto)
		{
			I = i;
			J = j;
			BaseId = baseId;
            GameBaseDto = baseDto;
		}

		public FieldPointDto() { }
	}
}