﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Bonuses;
using Rokolabs.Game.BllContracts.Entities.Enemies;
using Rokolabs.Game.BllContracts.Entities.Hurdles;
using Rokolabs.Game.BllContracts.Entities.Structures;
using Rokolabs.Game.BllContracts.Interfaces;
using Rokolabs.Game.DIResolver;
using Rokolabs.Game.Dto.Enums;

namespace Rokolabs.Game.UnitTests
{
	[TestClass]
	public class GameProcessorTest
	{
		private IConfigurationController _configurationController;
		private IGameController _gameController;
		private bool _isStateChanged;
		private GameState _gameState;

		[TestInitialize]
		public void TestInitialize()
		{
			DI.Configure();

			_configurationController = DI.Kernel.Get<IConfigurationController>();
			_gameController = DI.Kernel.Get<IGameController>();

			_configurationController.InitializeConfig(8, 9);
			_isStateChanged = false;
			_configurationController.AddOrUpdateGameEntity(new Hero(5), new SPoint(5, 5));
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(7, 7));
			_configurationController.SaveConfiguration();

			_gameController.StateChanged += delegate (GameState gameState)
			{
				_isStateChanged = true;
				_gameState = gameState;
			};
		}

		[TestCleanup]
		public void TestCleanup()
		{
			DI.Unbind();
		}

		[TestMethod]
		public void StartTest()
		{
			_gameController.Start();

			Assert.AreEqual(_gameController.GetGameStatus(), GameStatus.Runned);
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void StartTwiceTest()
		{
			_gameController.Start();
			_gameController.Start();
		}

		[TestMethod]
		public void StopTest()
		{
			_gameController.Start();
			_gameController.Stop();

			Assert.AreEqual(_gameController.GetGameStatus(), GameStatus.Stopped);
		}

		[TestMethod]
		public void ResetTest()
		{
			_gameController.Start();
			_gameController.Reset();

			Assert.AreEqual(_gameController.GetGameStatus(), GameStatus.Stopped);
		}

		[TestMethod]
		public void StateChangedEventTest()
		{
			uint heroIPoint = 5;
			uint heroJPoint = 5;
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Up);
			var gameBase = _gameState.FieldPoints.FirstOrDefault
				(x => x.I == heroIPoint && x.J == heroJPoint).GameBase;

			Assert.IsNull(gameBase);
			Assert.IsTrue(_isStateChanged);
		}

		[TestMethod]
		public void FieldPointsStateChangedCountTest()
		{
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Up);
			
			Assert.AreEqual(_gameState.FieldPoints.Count, 2);
		}

		[TestMethod]
		public void FieldPointsStateChangedPointsValueTest()
		{
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Up);
			
			Assert.AreEqual(_gameState.FieldPoints[0].I, 5);
			Assert.AreEqual(_gameState.FieldPoints[0].J, 4);
			Assert.AreEqual(_gameState.FieldPoints[1].I, 5);
			Assert.AreEqual(_gameState.FieldPoints[1].J, 5);
		}

		[TestMethod]
		public void FieldPointsStateChangedGameBaseTest()
		{
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Up);

			Assert.IsTrue(_gameState.FieldPoints[0].GameBase is Hero);
			Assert.IsNull(_gameState.FieldPoints[1].GameBase);
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void MoveHeroGameIsNotStartedTest()
		{
			_gameController.MoveHero(MoveDirection.Up);
		}

		[TestMethod]
		public void MoveHeroUpTest()
		{
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Up);
			
			Assert.AreEqual(_gameState.HeroPoint.I, 5);
			Assert.AreEqual(_gameState.HeroPoint.J, 4);
		}

		[TestMethod]
		public void MoveHeroDownTest()
		{
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Down);

			Assert.AreEqual(_gameState.HeroPoint.I, 5);
			Assert.AreEqual(_gameState.HeroPoint.J, 6);
		}

		[TestMethod]
		public void MoveHeroLeftTest()
		{
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Left);

			Assert.AreEqual(_gameState.HeroPoint.I, 4);
			Assert.AreEqual(_gameState.HeroPoint.J, 5);
		}

		[TestMethod]
		public void MoveHeroRightTest()
		{
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Right);

			Assert.AreEqual(_gameState.HeroPoint.I, 6);
			Assert.AreEqual(_gameState.HeroPoint.J, 5);
		}

		[TestMethod]
		public void MoveHeroNoneTest()
		{
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.None);

			Assert.AreEqual(_gameController.GetCurrentGameState().HeroPoint.I, 5);
			Assert.AreEqual(_gameController.GetCurrentGameState().HeroPoint.J, 5);
		}

		[TestMethod]
		public void InteractWithHurdleTest()
		{
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(4, 5));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Left);

			Assert.IsFalse(_isStateChanged);
			Assert.AreEqual(_gameController.GetCurrentGameState().HeroPoint.I, 5);
			Assert.AreEqual(_gameController.GetCurrentGameState().HeroPoint.J, 5);
		}

		[TestMethod]
		public void InteractWithRangeTest()
		{
			_configurationController.AddOrUpdateGameEntity(new Hero(5), new SPoint(0, 0));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Up);

			Assert.IsFalse(_isStateChanged);
			Assert.AreEqual(_gameController.GetCurrentGameState().HeroPoint.I, 0);
			Assert.AreEqual(_gameController.GetCurrentGameState().HeroPoint.J, 0);
		}

		[TestMethod]
		public void InteractWithEnemyTest()
		{
			_configurationController.AddOrUpdateGameEntity(new WolfEnemy(3), new SPoint(4, 5));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Left);

			Assert.AreEqual(_gameState.HeroPoint.I, 5);
			Assert.AreEqual(_gameState.HeroPoint.J, 5);
		}

		[TestMethod]
		public void TakeDamageFromEnemyTest()
		{
			_configurationController.AddOrUpdateGameEntity(new WolfEnemy(3), new SPoint(4, 5));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Left);

			Assert.IsTrue(((Hero)_gameState.HeroPoint.GameBase).HealthPoints == 2);
		}

		[TestMethod]
		public void InteractWithBonusTest()
		{
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(3), new SPoint(4, 5));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Left);

			Assert.AreEqual(_gameState.HeroPoint.I, 4);
			Assert.AreEqual(_gameState.HeroPoint.J, 5);
		}

		[TestMethod]
		public void EnemyInteractWithEnemyTest()
		{
			var bear = new BearEnemy(2);
			var wolf = new WolfEnemy(3);
			EnemyAlgorithmHelper.SetAlgorithm(bear, MoveDirection.Left);
			EnemyAlgorithmHelper.SetAlgorithm(wolf, MoveDirection.None);
			_configurationController.AddOrUpdateGameEntity(bear, new SPoint(0, 1));
			_configurationController.AddOrUpdateGameEntity(wolf, new SPoint(0, 0));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.None);

			Assert.IsFalse(_isStateChanged);
		}

		[TestMethod]
		public void EnemyInteractWithEnemyTowardsTest()
		{
			var bear = new BearEnemy(2);
			var wolf = new WolfEnemy(3);
			EnemyAlgorithmHelper.SetAlgorithm(bear, MoveDirection.Left);
			EnemyAlgorithmHelper.SetAlgorithm(wolf, MoveDirection.Right);
			_configurationController.AddOrUpdateGameEntity(bear, new SPoint(1, 0));
			_configurationController.AddOrUpdateGameEntity(wolf, new SPoint(0, 0));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.None);

			Assert.IsFalse(_isStateChanged);
		}

		[TestMethod]
		public void EnemyInteractWithHeroTest()
		{
			var bear = new BearEnemy(2);
			EnemyAlgorithmHelper.SetAlgorithm(bear, MoveDirection.Left);
			_configurationController.AddOrUpdateGameEntity(bear, new SPoint(6, 5));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.None);

			Assert.IsTrue(_isStateChanged);
			Assert.IsFalse(_gameState.FieldPoints.Any());
			Assert.AreEqual(_gameState.HealthPoints, 3);
		}

		[TestMethod]
		public void TwoEnemiesInteractWithHeroTest()
		{
			var bear = new BearEnemy(2);
			var wolf = new WolfEnemy(3);
			EnemyAlgorithmHelper.SetAlgorithm(bear, MoveDirection.Left);
			EnemyAlgorithmHelper.SetAlgorithm(wolf, MoveDirection.Right);
			_configurationController.AddOrUpdateGameEntity(bear, new SPoint(6, 5));
			_configurationController.AddOrUpdateGameEntity(wolf, new SPoint(4, 5));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.None);

			Assert.IsTrue(_isStateChanged);
			Assert.IsFalse(_gameState.FieldPoints.Any());
			Assert.AreEqual(_gameState.HealthPoints, 0);
			Assert.IsTrue(_gameState.IsGameOver);
		}

		[TestMethod]
		public void EnemyInteractWithHurdleTest()
		{
			var bear = new BearEnemy(2);
			EnemyAlgorithmHelper.SetAlgorithm(bear, MoveDirection.Left);
			_configurationController.AddOrUpdateGameEntity(bear, new SPoint(6, 4));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(5, 4));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.None);

			Assert.IsFalse(_isStateChanged);
		}

		[TestMethod]
		public void EnemyInteractWithRangeTest()
		{
			var bear = new BearEnemy(2);
			EnemyAlgorithmHelper.SetAlgorithm(bear, MoveDirection.Left);
			_configurationController.AddOrUpdateGameEntity(bear, new SPoint(0, 0));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.None);

			Assert.IsFalse(_isStateChanged);
		}

		[TestMethod]
		public void EnemyInteractWithBonusTest()
		{
			var wolf = new WolfEnemy(2);
			EnemyAlgorithmHelper.SetAlgorithm(wolf, MoveDirection.Left);
			_configurationController.AddOrUpdateGameEntity(wolf, new SPoint(6, 4));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(3), new SPoint(5, 4));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.None);

			Assert.IsFalse(_isStateChanged);
		}

		[TestMethod]
		public void EatableEnemyInteractWithBonusTest()
		{
			var bear = new BearEnemy(2);
			EnemyAlgorithmHelper.SetAlgorithm(bear, MoveDirection.Left);
			_configurationController.AddOrUpdateGameEntity(bear, new SPoint(6, 4));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(3), new SPoint(5, 4));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.None);

			Assert.AreEqual(bear.Damage, 5);
			Assert.IsTrue(_gameState.FieldPoints.FirstOrDefault(x => x.I == 5 && x.J == 4).GameBase is BearEnemy);
		}

		[TestMethod]
		public void EatableEnemyInteractWithBonusCounterTest()
		{
			var bear = new BearEnemy(2);
			EnemyAlgorithmHelper.SetAlgorithm(bear, MoveDirection.Left);
			_configurationController.AddOrUpdateGameEntity(bear, new SPoint(6, 4));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(3), new SPoint(5, 4));
			_configurationController.SaveConfiguration();
			_gameController.Start();

			Assert.AreEqual(_gameController.GetCurrentGameState().BonusesCount, 2);
			_gameController.MoveHero(MoveDirection.None);

			Assert.AreEqual(_gameState.BonusesCount, 1);
		}

		[TestMethod]
		public void EatBonusEffectTest()
		{
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(3), new SPoint(4, 5));
			_configurationController.RemoveGameEntity(new SPoint(7, 7));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Left);

			Assert.AreEqual(_gameState.HealthPoints, 8);
		}

		[TestMethod]
		public void GameOverWhenBonusEndedTest()
		{
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(3), new SPoint(4, 5));
			_configurationController.RemoveGameEntity(new SPoint(7, 7));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Left);

			Assert.IsTrue(_gameState.IsGameOver);
		}

		[TestMethod]
		public void BonusesCounterTest()
		{
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(3), new SPoint(4, 5));
			_configurationController.SaveConfiguration();
			_gameController.Start();

			Assert.AreEqual(_gameController.GetCurrentGameState().BonusesCount, 2);
			_gameController.MoveHero(MoveDirection.Left);

			Assert.AreEqual(_gameState.BonusesCount, 1);
			Assert.AreEqual(_gameController.GetCurrentGameState().BonusesCount, 1);
		}

		[TestMethod]
		public void GameOverWhenHeroDiedTest()
		{
			_configurationController.AddOrUpdateGameEntity(new BearEnemy(20), new SPoint(4, 5));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Left);

			Assert.IsTrue(_gameState.IsGameOver);
		}

		[TestMethod]
		public void GameStopedWhenBonusEndedTest()
		{
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(3), new SPoint(4, 5));
			_configurationController.RemoveGameEntity(new SPoint(7, 7));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Left);

			Assert.AreEqual(_gameController.GetGameStatus(), GameStatus.Stopped);
		}

		[TestMethod]
		public void GameStopedWhenHeroDiedTest()
		{
			_configurationController.AddOrUpdateGameEntity(new BearEnemy(20), new SPoint(4, 5));
			_configurationController.SaveConfiguration();
			_gameController.Start();
			_gameController.MoveHero(MoveDirection.Left);

			Assert.AreEqual(_gameController.GetGameStatus(), GameStatus.Stopped);
		}

		[TestMethod]
		public void HeroHealthPointCounterTest()
		{
			_configurationController.AddOrUpdateGameEntity(new WolfEnemy(2), new SPoint(4, 5));
			_configurationController.SaveConfiguration();
			_gameController.Start();

			Assert.AreEqual(_gameController.GetCurrentGameState().HealthPoints, 5);
			_gameController.MoveHero(MoveDirection.Left);

			Assert.AreEqual(_gameController.GetCurrentGameState().HealthPoints, 3);
			Assert.AreEqual(_gameState.HealthPoints, 3);
		}
	}
}