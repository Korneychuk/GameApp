﻿using Moq;
using Rokolabs.Game.BllContracts.Entities.Enemies;
using Rokolabs.Game.BllContracts.Interfaces;
using Rokolabs.Game.Dto.Enums;

namespace Rokolabs.Game.UnitTests
{
	internal static class EnemyAlgorithmHelper
	{
		internal static void SetAlgorithm(Enemy enemy, MoveDirection direction)
		{
			var mock = new Mock<IMoveStrategy>();
			mock.Setup(x => x.CalculateNextMoveDirection()).Returns(direction);
			enemy.MoveStrategy = mock.Object;
		}
	}
}