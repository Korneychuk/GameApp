﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Bonuses;
using Rokolabs.Game.BllContracts.Entities.Hurdles;
using Rokolabs.Game.BllContracts.Entities.Structures;
using Rokolabs.Game.BllContracts.Interfaces;
using Rokolabs.Game.DIResolver;

namespace Rokolabs.Game.UnitTests
{
	[TestClass]
	public class ConfigurationManagerTest
	{
		private IConfigurationController _configurationController;
		private IFieldController _fieldController;

		[TestInitialize]
		public void TestInitialize()
		{
			DI.Configure();

			_fieldController = DI.Kernel.Get<IFieldController>();
			_configurationController = DI.Kernel.Get<IConfigurationController>();
			_configurationController.InitializeConfig(6, 7);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			DI.Unbind();
		}

		[TestMethod]
		public void InitializeWithValidSizeTest()
		{
			_configurationController.InitializeConfig(5, 10);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void InitializeWithNulldSizeTest()
		{
			_configurationController.InitializeConfig(0, 5);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void InitializeWithNegativedSizeTest()
		{
			_configurationController.InitializeConfig(5, -5);
		}

		[TestMethod]
		public void SaveValidConfigTest()
		{
			_configurationController.AddOrUpdateGameEntity(new Hero(5), new SPoint(0, 0));
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(0, 1));

			Assert.IsTrue(_configurationController.SaveConfiguration());
		}

		[TestMethod]
		public void SaveConfigWithoutBonusTest()
		{
			_configurationController.AddOrUpdateGameEntity(new Hero(5), new SPoint(0, 0));

			Assert.IsFalse(_configurationController.SaveConfiguration());
		}

		[TestMethod]
		public void SaveConfigWithoutHeroTest()
		{
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(0, 1));

			Assert.IsFalse(_configurationController.SaveConfiguration());
		}

		[TestMethod]
		public void AddEntityCurrentGameStateCountTest()
		{
			_configurationController.AddOrUpdateGameEntity(new Hero(5), new SPoint(0, 0));
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(0, 1));
			_configurationController.SaveConfiguration();
			var gamestate = _fieldController.GetCurrentGameState();

			Assert.AreEqual(gamestate.FieldPoints.Count, 42);
		}

		[TestMethod]
		public void AddEntityCurrentGameStatePositionsTest()
		{
			_configurationController.AddOrUpdateGameEntity(new Hero(5), new SPoint(2, 3));
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(0, 1));
			_configurationController.SaveConfiguration();
			var gamestate = _fieldController.GetCurrentGameState();

			Assert.AreEqual(gamestate.FieldPoints[17].I, 2);
			Assert.AreEqual(gamestate.FieldPoints[17].J, 3);
			Assert.AreEqual(gamestate.FieldPoints[1].I, 0);
			Assert.AreEqual(gamestate.FieldPoints[1].J, 1);
		}

		[TestMethod]
		public void AddEntityCurrentGameStateGameBaseTest()
		{
			var hero = new Hero(5);
			var bonus = new AppleBonus(5);
			_configurationController.AddOrUpdateGameEntity(hero, new SPoint(0, 0));
			_configurationController.AddOrUpdateGameEntity(bonus, new SPoint(0, 1));
			_configurationController.SaveConfiguration();
			var gamestate = _fieldController.GetCurrentGameState();

			Assert.AreEqual(gamestate.FieldPoints[0].GameBase, hero);
			Assert.AreEqual(gamestate.FieldPoints[1].GameBase, bonus);

		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void AddEntityWithInvalidPositionTest()
		{
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(10, 1));
		}

		[TestMethod]
		public void UpdatePointTest()
		{
			_configurationController.AddOrUpdateGameEntity(new Hero(5), new SPoint(0, 0));
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(0, 1));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(5), new SPoint(0, 1));
			_configurationController.SaveConfiguration();
			var fieldPoints = _fieldController.GetCurrentGameState().FieldPoints;
			var apple = fieldPoints.FirstOrDefault(x => x.GameBase is AppleBonus);
			var raspberry = fieldPoints.FirstOrDefault(x => x.GameBase is RaspberryBonus);

			Assert.IsNull(apple);
			Assert.IsNotNull(raspberry);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void AddHeroZeroHealthTest()
		{
			var hero = new Hero(0);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void AddhHeroNegativeHealthTest()
		{
			var hero = new Hero(-5);
		}

		[TestMethod]
		public void AddTwoHeroTest()
		{
			_configurationController.AddOrUpdateGameEntity(new Hero(5), new SPoint(0, 0));
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(0, 1));
			_configurationController.AddOrUpdateGameEntity(new Hero(6), new SPoint(0, 2));

			Assert.IsTrue(_configurationController.SaveConfiguration());
			var gamestate = _fieldController.GetCurrentGameState();
			Assert.IsNull(gamestate.FieldPoints[0].GameBase);
			Assert.IsTrue(gamestate.FieldPoints[2].GameBase is Hero);
		}

		[TestMethod]
		public void RemoveEntityTest()
		{
			_configurationController.AddOrUpdateGameEntity(new Hero(5), new SPoint(0, 0));
			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(0, 1));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(0, 2));
			_configurationController.RemoveGameEntity(new SPoint(0, 2));

			Assert.IsTrue(_configurationController.SaveConfiguration());
			var gamestate = _fieldController.GetCurrentGameState();

			Assert.IsNull(gamestate.FieldPoints[3].GameBase);
		}
	}
}