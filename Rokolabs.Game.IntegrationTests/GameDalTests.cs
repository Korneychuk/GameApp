﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Bonuses;
using Rokolabs.Game.BllContracts.Entities.Enemies;
using Rokolabs.Game.BllContracts.Entities.Hurdles;
using Rokolabs.Game.BllContracts.Entities.Structures;
using Rokolabs.Game.BllContracts.Interfaces;
using Rokolabs.Game.DIResolver;
using Ninject;

namespace Rokolabs.Game.IntegrationTests
{
	[TestClass]
	public class GameDalTests
	{
		private IConfigurationController _configurationController;
		private IFieldController _fieldController;
		private IConfigurationLoader _configurationLoader;

		[TestInitialize]
		public void TestInitialize()
		{
			DI.Configure();
			_fieldController = DI.Kernel.Get<IFieldController>();
			_configurationController = DI.Kernel.Get<IConfigurationController>();
			_configurationLoader = DI.Kernel.Get<IConfigurationLoader>();
		}

		[TestCleanup]
		public void TestCleanup()
		{
			DI.Unbind();
		}

		[TestMethod]
		public void InitializeThenLoadBaseTest()
		{
			try
			{
				InitializeConfig();
				_configurationLoader.UpdateConfiguration(_fieldController.GetCurrentGameState().FieldPoints);
			}
			catch (SqlException e)
			{
				Trace.WriteLine("Base were alredy initialized");
                Assert.Fail();
			}

			var fieldPoints = _configurationLoader.LoadConfiguration().ToList();
			var hero = fieldPoints.FirstOrDefault(x => x.I == 5 && x.J == 5).GameBase;

			Assert.IsInstanceOfType(hero, typeof(Hero));
		}

		[TestMethod]
		public void RecordsTest()
		{
			_configurationLoader.UpdateRecordsTable(new Record("TestUser1", 50, 5, 150));
			_configurationLoader.UpdateRecordsTable(new Record("TestUser2", 20, 2, 10));

			var oneRecord = _configurationLoader.GetRecordsTable(1);
			var twoRecords = _configurationLoader.GetRecordsTable(2);

			Assert.AreEqual(oneRecord.Count, 1);
			Assert.AreEqual(twoRecords.Count, 2);
			Assert.AreEqual(oneRecord[0].PlayerName, "TestUser1");
		}

		private void InitializeConfig()
		{
			_configurationController.InitializeConfig(10, 10);
			_configurationController.AddOrUpdateGameEntity(new Hero(50), new SPoint(5, 5));

			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(0, 3));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(4), new SPoint(0, 2));
			_configurationController.AddOrUpdateGameEntity(new WolfEnemy(4), new SPoint(0, 5));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(0, 8));
			_configurationController.AddOrUpdateGameEntity(new TreeHurdle(), new SPoint(0, 4));

			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(1, 8));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(4), new SPoint(1, 9));
			_configurationController.AddOrUpdateGameEntity(new BearEnemy(20), new SPoint(1, 3));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(1, 5));
			_configurationController.AddOrUpdateGameEntity(new TreeHurdle(), new SPoint(1, 0));

			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(2, 2));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(4), new SPoint(2, 6));
			_configurationController.AddOrUpdateGameEntity(new WolfEnemy(4), new SPoint(2, 9));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(2, 4));
			_configurationController.AddOrUpdateGameEntity(new TreeHurdle(), new SPoint(2, 5));

			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(3, 6));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(4), new SPoint(3, 3));
			_configurationController.AddOrUpdateGameEntity(new BearEnemy(6), new SPoint(3, 5));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(3, 7));
			_configurationController.AddOrUpdateGameEntity(new TreeHurdle(), new SPoint(3, 9));

			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(4, 7));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(4), new SPoint(4, 1));
			_configurationController.AddOrUpdateGameEntity(new WolfEnemy(4), new SPoint(4, 5));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(4, 8));
			_configurationController.AddOrUpdateGameEntity(new TreeHurdle(), new SPoint(4, 6));

			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(5, 0));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(4), new SPoint(5, 9));
			_configurationController.AddOrUpdateGameEntity(new BearEnemy(6), new SPoint(5, 1));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(5, 3));
			_configurationController.AddOrUpdateGameEntity(new TreeHurdle(), new SPoint(5, 8));

			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(6, 3));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(4), new SPoint(6, 5));
			_configurationController.AddOrUpdateGameEntity(new WolfEnemy(4), new SPoint(6, 7));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(6, 1));
			_configurationController.AddOrUpdateGameEntity(new TreeHurdle(), new SPoint(6, 2));

			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(7, 4));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(4), new SPoint(7, 8));
			_configurationController.AddOrUpdateGameEntity(new BearEnemy(6), new SPoint(7, 5));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(7, 0));
			_configurationController.AddOrUpdateGameEntity(new TreeHurdle(), new SPoint(7, 6));

			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(8, 9));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(4), new SPoint(8, 3));
			_configurationController.AddOrUpdateGameEntity(new WolfEnemy(4), new SPoint(8, 8));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(8, 5));
			_configurationController.AddOrUpdateGameEntity(new TreeHurdle(), new SPoint(8, 2));

			_configurationController.AddOrUpdateGameEntity(new AppleBonus(5), new SPoint(9, 1));
			_configurationController.AddOrUpdateGameEntity(new RaspberryBonus(4), new SPoint(9, 7));
			_configurationController.AddOrUpdateGameEntity(new BearEnemy(6), new SPoint(9, 2));
			_configurationController.AddOrUpdateGameEntity(new RockHurdle(), new SPoint(9, 4));
			_configurationController.AddOrUpdateGameEntity(new TreeHurdle(), new SPoint(9, 6));

			_configurationController.SaveConfiguration();
		}
	}
}