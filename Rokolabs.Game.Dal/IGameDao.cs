﻿using System.Collections.Generic;
using Rokolabs.Game.Dto;

namespace Rokolabs.Game.SqlDal
{
	public interface IGameDao
	{
		IEnumerable<FieldPointDto> LoadConfiguration();

		void UpdateConfiguration(IEnumerable<FieldPointDto> fieldPoints);

		void UpdateRecordsTable(RecordDto record);

		IEnumerable<RecordDto> GetRecordsTable(int count);
	}
}