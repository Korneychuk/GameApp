﻿using Ninject;
using Rokolabs.Game.BllContracts.Interfaces;
using Rokolabs.Game.Core;
using Rokolabs.Game.DalContracts;
using Rokolabs.Game.SqlDal;

namespace Rokolabs.Game.DIResolver
{
	public class DI
	{
		public static IKernel Kernel { get; }

		static DI()
		{
			Kernel = new StandardKernel();
		}

		public static void Configure()
		{
			Kernel.Bind<IFieldController>().To<FieldController>().InSingletonScope();
			Kernel.Bind<IGameDao>().To<GameDao>().InSingletonScope();
			Kernel.Bind<IGameController>().To<GameController>().InSingletonScope();
			Kernel.Bind<IConfigurationController>().To<ConfigurationController>().InSingletonScope();
			Kernel.Bind<IConfigurationLoader>().To<ConfigurationLoader>().InSingletonScope();
		}

		public static void Unbind()
		{
			Kernel.Unbind<IFieldController>();
			Kernel.Unbind<IGameDao>();
			Kernel.Unbind<IGameController>();
			Kernel.Unbind<IConfigurationController>();
			Kernel.Unbind<IConfigurationLoader>();
		}
	}
}