﻿using System.Collections.Generic;
using System.Linq;
using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Field;
using Rokolabs.Game.BllContracts.Interfaces;
using Rokolabs.Game.BllContracts.Mapping;
using Rokolabs.Game.SqlDal;

namespace Rokolabs.Game.Core
{
	public class ConfigurationLoader : IConfigurationLoader
	{
		private readonly IGameDao _gameDao;

		public ConfigurationLoader(IGameDao gameDao)
		{
			_gameDao = gameDao;
		}

		public List<FieldPoint> LoadConfiguration()
		{
			var fieldPointsDto = _gameDao.LoadConfiguration();
			return fieldPointsDto.Select(Mapper.FieldPointFromDto).ToList();
		}

		public void UpdateConfiguration(IEnumerable<FieldPoint> fieldPoints)
		{
			var fieldPointsDto = fieldPoints.Select(Mapper.DtoFromFieldPoint).ToList();
			_gameDao.UpdateConfiguration(fieldPointsDto);
		}

		public void UpdateRecordsTable(Record record)
		{
			_gameDao.UpdateRecordsTable(Mapper.DtoFromRecord(record));
		}

		public List<Record> GetRecordsTable(int count)
		{
			return _gameDao.GetRecordsTable(count).Select(Mapper.RecordFromDto).ToList();
		}
	}
}