﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Bonuses;
using Rokolabs.Game.BllContracts.Entities.Structures;
using Rokolabs.Game.BllContracts.Interfaces;

namespace Rokolabs.Game.Core
{
	public class ConfigurationController: IConfigurationController
	{
		private int _fieldWidth;
		private int _fieldHight;
		private readonly Dictionary<SPoint, GameBase> _entities;
		private readonly IFieldController _fieldController;

		public ConfigurationController(IFieldController fieldController)
		{
			_fieldController = fieldController;
			_entities = new Dictionary<SPoint, GameBase>();
		}

		public void InitializeConfig(int fieldWidth, int fieldHight)
		{
			if (fieldHight > 0 && fieldWidth > 0)
			{
				_fieldWidth = fieldWidth;
				_fieldHight = fieldHight;
				_fieldController.SetGameField(fieldWidth, fieldHight);
			}
			else
				throw new ArgumentException("Field width and field hight must be a natural number");
		}

		public void AddOrUpdateGameEntity(GameBase gameBase, SPoint position)
		{
			CheckPosition(position);
			if (gameBase is Hero)
			{
				var heroEntity = _entities.FirstOrDefault(x => x.Value is Hero);
				if (heroEntity.Value != null)
				{
					_entities.Remove(heroEntity.Key);
				}
			}
			else
			{
				var existingEntity = _entities.FirstOrDefault(x => Equals(x.Key, position));
				if (existingEntity.Value != null)
				{
					_entities.Remove(position);
				}
			}

			_entities.Add(position, gameBase);
		}

		public void RemoveGameEntity(SPoint position)
		{
			CheckPosition(position);
			_entities.Remove(position);
		}

		public bool SaveConfiguration()
		{
			var isHeroAdded = false;
			var isBonusAdded = false;
			ClearGameField();
			foreach (var entity in _entities)
			{
				var fieldPoint = _fieldController.GetCurrentGameState().FieldPoints
					.FirstOrDefault(x => entity.Key.I == x.I && entity.Key.J == x.J);
				if (fieldPoint != null)
				{
					fieldPoint.GameBase = entity.Value;
				}
				else
				{
					return false;
				}
				if (entity.Value is Hero) isHeroAdded = true;
				if (entity.Value is Bonus) isBonusAdded = true;
			}

			return isBonusAdded && isHeroAdded;
		}

		public void ClearGameField()
		{
			_fieldController.GetCurrentGameState().FieldPoints.ForEach(x => x.GameBase = null);
		}

		private void CheckPosition(SPoint position)
		{
			if (position.I >= _fieldWidth || position.J >= _fieldHight)
			{
				throw new ArgumentOutOfRangeException(
					$"Position x:{position.I} y:{position.J} is out of field range ({_fieldWidth}, {_fieldHight}).");
			}
		}
	}
}