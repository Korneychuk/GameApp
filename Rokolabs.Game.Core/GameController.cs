﻿using System;
using Rokolabs.Game.BllContracts;
using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Field;
using Rokolabs.Game.BllContracts.Interfaces;
using Rokolabs.Game.Dto.Enums;

namespace Rokolabs.Game.Core
{
	public class GameController : IGameController
	{
		private readonly IConfigurationController _configurationController;
		private readonly IFieldController _fieldController;
		private GameStatus _gameStatus;

		public FieldPoint HeroPoint => _fieldController.GetCurrentGameState().HeroPoint;

		public event FieldStateHandler StateChanged;

		public GameController(IConfigurationController configurationController, IFieldController fieldController)
		{
			_configurationController = configurationController;
			_fieldController = fieldController;
		}

		public GameState GetCurrentGameState() => _fieldController.GetCurrentGameState();

		public GameStatus GetGameStatus() => _gameStatus;

		public void Start()
		{
			if (_gameStatus != GameStatus.Runned)
			{
				_fieldController.Initialize();
				_gameStatus = GameStatus.Runned;
				_fieldController.FieldChanged += OnFieldChanged;
			}
			else
				throw new InvalidOperationException("The game were already started.");
		}

		public void MoveHero(MoveDirection moveDirection)
		{
			if (_gameStatus == GameStatus.Runned)
			{
				_fieldController.Iterate(moveDirection);
			}
			else
				throw new InvalidOperationException("The game isn't started.");
		}

		public void Stop()
		{
			_gameStatus = GameStatus.Stopped;
		}

		public void Reset()
		{
			Stop();
			_configurationController.ClearGameField();
		}

		private void OnFieldChanged(GameState gameState)
		{
			if (gameState.IsGameOver)
			{
				Stop();
			}
			StateChanged?.Invoke(gameState);
		}
	}
}