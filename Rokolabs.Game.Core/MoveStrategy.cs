﻿using System;
using Rokolabs.Game.BllContracts.Interfaces;
using Rokolabs.Game.Dto.Enums;

namespace Rokolabs.Game.Core
{
	internal class MoveStrategy : IMoveStrategy
	{
		public MoveDirection CalculateNextMoveDirection()
		{
			var random = new Random();
			return (MoveDirection)random.Next(1, 4);
		}
	}
}