﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rokolabs.Game.BllContracts;
using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Bonuses;
using Rokolabs.Game.BllContracts.Entities.Enemies;
using Rokolabs.Game.BllContracts.Entities.Field;
using Rokolabs.Game.BllContracts.Entities.Structures;
using Rokolabs.Game.BllContracts.Interfaces;
using Rokolabs.Game.Dto.Enums;

namespace Rokolabs.Game.Core
{
	public class FieldController : IFieldController
	{
		private List<FieldPoint> _changedPoints;
		private List<FieldPoint> _enemiesPoints;
		private int _hight;
		private int _width;
		private int _bonusesCount;
		private FieldPoints _fieldPoints;
		private FieldPoint _heroPoint;

		private Hero Hero => _heroPoint?.GameBase as Hero;

		public event FieldStateHandler FieldChanged;

		public GameState GetCurrentGameState() => new GameState(_heroPoint, _bonusesCount,
			_fieldPoints.GetFieldPoints().ToList());

		public void Iterate(MoveDirection moveDirection)
		{
			MoveHero(moveDirection);
			MoveEnemies();
			NotifyIfStateChanged();
		}

		public void SetGameField(int fieldWidth, int fieldHight)
		{
			_width = fieldWidth;
			_hight = fieldHight;
			_fieldPoints = new FieldPoints(fieldWidth, fieldHight);
		}

		public void Initialize()
		{
			_enemiesPoints = new List<FieldPoint>();
			foreach (var fieldPoint in _fieldPoints.GetFieldPoints())
			{
				if (fieldPoint.GameBase is Enemy enemy)
				{
					_enemiesPoints.Add(fieldPoint);
					if (enemy.MoveStrategy == null)
					{
						enemy.MoveStrategy = new MoveStrategy();
					}
				}
				if (fieldPoint.GameBase is Hero hero)
				{
					_heroPoint = fieldPoint;
					hero.HeroTakeDamage += OnHeroTakenDamage;
				}
				if (fieldPoint.GameBase is Bonus bonus)
				{
					_bonusesCount++;
					bonus.BonusEaten += OnBonusEaten;
				}
			}
		}

		private void MoveHero(MoveDirection moveDirection)
		{
			_changedPoints = new List<FieldPoint>();
			var newFieldPoint = GetNewPointByDirection(_heroPoint, moveDirection);
			var heroNewFieldPoint = Hero.Move(newFieldPoint, _heroPoint);
			SetEntityNewPos(heroNewFieldPoint, _heroPoint);
		}

		private void MoveEnemies()
		{
			foreach (var enemyPoint in _enemiesPoints)
			{
				var enemy = (Enemy) enemyPoint.GameBase;
				var newFieldPoint = GetNewPointByDirection(enemyPoint, enemy.MoveStrategy.CalculateNextMoveDirection());
				if (newFieldPoint != enemyPoint)
				{
					newFieldPoint = enemy.Move(newFieldPoint, enemyPoint);
					SetEntityNewPos(newFieldPoint, enemyPoint);
				}
			}
		}

		private void NotifyIfStateChanged(bool isStateChanged = false)
		{
			if (_changedPoints.Any() || isStateChanged)
			{
				var gameState = new GameState(_heroPoint, _bonusesCount, _changedPoints);
				FieldChanged?.Invoke(gameState);
			}
		}

		private void OnBonusEaten()
		{
			_bonusesCount--;
			NotifyIfStateChanged(isStateChanged: true);
		}

		private void OnHeroTakenDamage()
		{
			NotifyIfStateChanged(isStateChanged: true);
		}

		private void SetEntityNewPos(FieldPoint newFieldPoint, FieldPoint oldFieldPoint)
		{
			if (newFieldPoint != oldFieldPoint)
			{
				newFieldPoint.GameBase = oldFieldPoint.GameBase;
				oldFieldPoint.GameBase = null;
				if (newFieldPoint.GameBase is Hero)
				{
					_heroPoint = newFieldPoint;
				}

				_changedPoints.Add(newFieldPoint);
				_changedPoints.Add(oldFieldPoint);
			}
		}

		private FieldPoint GetNewPointByDirection(FieldPoint oldFieldPoint, MoveDirection moveDirection)
		{
			int i = oldFieldPoint.I;
			int j = oldFieldPoint.J;
			switch (moveDirection)
			{
				case MoveDirection.None:
					break;
				case MoveDirection.Up:
					j--;
					break;
				case MoveDirection.Down:
					j++;
					break;
				case MoveDirection.Left:
					i--;
					break;
				case MoveDirection.Right:
					i++;
					break;
				default:
					throw new ArgumentException($"Incorrect direction: {moveDirection}");
			}

			var newPosition = new SPoint(i, j);
			return IsPositionValid(newPosition) ? _fieldPoints.GetByAddress(newPosition) : oldFieldPoint;
		}

		private bool IsPositionValid(SPoint position)
		{
			return !(position.I >= _width || position.J >= _hight || position.I < 0 || position.J < 0);
		}
	}
}