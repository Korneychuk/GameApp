﻿using Rokolabs.Game.BllContracts.Entities.Bonuses;
using Rokolabs.Game.BllContracts.Interfaces;

namespace Rokolabs.Game.BllContracts.Entities.Enemies
{
	public class BearEnemy : Enemy, IEatable
	{
		public BearEnemy(int damage) : base(damage)
		{
		}

		public void EatBonus(Bonus bonus)
		{
			Damage += bonus.BonusStrength;
			bonus.IsBonusEaten = true;
		}
	}
}