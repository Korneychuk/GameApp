﻿using Rokolabs.Game.BllContracts.Entities.Bonuses;
using Rokolabs.Game.BllContracts.Entities.Field;
using Rokolabs.Game.BllContracts.Entities.Hurdles;
using Rokolabs.Game.BllContracts.Interfaces;

namespace Rokolabs.Game.BllContracts.Entities.Enemies
{
	public abstract class Enemy : GameBase, IMoveable
	{
		protected Enemy(int damage)
		{
			Damage = damage;
		}

		public IMoveStrategy MoveStrategy { get; set; }

		public int Damage { get; protected set; }

		public virtual FieldPoint Move(FieldPoint newFieldPoint, FieldPoint oldFieldPoint)
		{
			switch (newFieldPoint.GameBase)
			{
				case Hurdle hurdle:
					return InteractWithHurdle(oldFieldPoint);
				case Enemy enemy:
					return InteractWithEnemy(oldFieldPoint);
				case Bonus bonus:
					return InteractWithBonus(newFieldPoint, oldFieldPoint);
				case Hero hero:
					return InteractWithHero(hero, oldFieldPoint);
				default:
					return newFieldPoint;
			}
		}

		protected virtual FieldPoint InteractWithHurdle (FieldPoint oldFieldPoint)
		{
			return oldFieldPoint;
		}

		protected virtual FieldPoint InteractWithEnemy(FieldPoint oldFieldPoint)
		{
			return oldFieldPoint;
		}

		protected virtual FieldPoint InteractWithBonus(FieldPoint bonusPoint, FieldPoint oldFieldPoint)
		{
			if (this is IEatable monstr)
			{
				monstr.EatBonus(bonusPoint.GameBase as Bonus);
				return bonusPoint;
			}
			return oldFieldPoint;
		}

		protected virtual FieldPoint InteractWithHero(Hero hero, FieldPoint oldFieldPoint)
		{
			hero.OnTakeDamage(Damage);
			return oldFieldPoint;
		}
	}
}