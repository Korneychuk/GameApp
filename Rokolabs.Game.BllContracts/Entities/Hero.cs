﻿using System;
using Rokolabs.Game.BllContracts.Entities.Bonuses;
using Rokolabs.Game.BllContracts.Entities.Enemies;
using Rokolabs.Game.BllContracts.Entities.Field;
using Rokolabs.Game.BllContracts.Entities.Hurdles;
using Rokolabs.Game.BllContracts.Interfaces;

namespace Rokolabs.Game.BllContracts.Entities
{
	public class Hero : GameBase, IMoveable, IEatable
	{
		public Hero(int healthPoints)
		{
			if (healthPoints > 0)
			{
				HealthPoints = healthPoints;
			}
			else
				throw new ArgumentException("Hero's HP must be a natural number");
		}

		public int HealthPoints { get; private set; }

		public event HeroTakeDamageEventHandler HeroTakeDamage;

		public delegate void HeroTakeDamageEventHandler();

		public void EatBonus(Bonus bonus)
		{
			HealthPoints += bonus.BonusStrength;
			bonus.IsBonusEaten = true;
		}

		public FieldPoint Move(FieldPoint newFieldPoint, FieldPoint oldFieldPoint)
		{
			switch (newFieldPoint.GameBase)
			{
				case Hurdle hurdle:
					return InteractWithHurdle(oldFieldPoint);
				case Enemy enemy:
					return InteractWithEnemy(enemy, oldFieldPoint);
				case Bonus bonus:
					return InteractWithBonus(newFieldPoint, bonus);
				default:
					return newFieldPoint;
			}
		}

		internal void OnTakeDamage(int damageValue)
		{
			HealthPoints -= damageValue;
			HeroTakeDamage?.Invoke();
		}

		private FieldPoint InteractWithBonus(FieldPoint newFieldPoint, Bonus bonus)
		{
			EatBonus(bonus);
			return newFieldPoint;
		}

		private FieldPoint InteractWithEnemy(Enemy enemy, FieldPoint oldFieldPoint)
		{
			OnTakeDamage(enemy.Damage);
			return oldFieldPoint;
		}

		private FieldPoint InteractWithHurdle(FieldPoint oldFieldPoint)
		{
			return oldFieldPoint;
		}
	}
}