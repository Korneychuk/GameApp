﻿namespace Rokolabs.Game.BllContracts.Entities.Structures
{
	public struct SPoint
	{
		public int I;
		public int J;

		public SPoint(int i, int j)
		{
			I = i;
			J = j;
		}
	}
}