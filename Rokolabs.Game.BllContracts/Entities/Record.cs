﻿namespace Rokolabs.Game.BllContracts.Entities
{
	public class Record
	{
		public string PlayerName { get; }

		public long Score { get; }

		public Record(string playerName, int bonusesCount, int healthPoints, int turns)
		{
			PlayerName = playerName;
			Score = CalculateScore(bonusesCount, healthPoints, turns);
		}

		public Record(string playerName, long score)
		{
			PlayerName = playerName;
			Score = score;
		}

		private long CalculateScore(int bonusesCount, int healthPoints, int turns)
		{
			return bonusesCount * healthPoints * 1000 / turns;
		}
	}
}