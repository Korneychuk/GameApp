﻿using System.Collections.Generic;
using Rokolabs.Game.BllContracts.Entities.Field;

namespace Rokolabs.Game.BllContracts.Entities
{
	public class GameState
	{
		public GameState(FieldPoint heroPoint, int bonusesCount, List<FieldPoint> fieldPoints)
		{
			FieldPoints = fieldPoints;
			if (heroPoint?.GameBase is Hero hero)
			{
				HealthPoints = hero.HealthPoints;
				HeroPoint = heroPoint;
			}
			BonusesCount = bonusesCount;
		}

		public List<FieldPoint> FieldPoints { get; set; }

		public bool IsGameOver => HealthPoints <= 0 || BonusesCount <= 0;

		public FieldPoint HeroPoint { get; }

		public int HealthPoints { get; }

		public int BonusesCount { get; }
	}
}