﻿namespace Rokolabs.Game.BllContracts.Entities.Bonuses
{
	public abstract class Bonus : GameBase
	{
		protected Bonus(int bonusStrength)
		{
			BonusStrength = bonusStrength;
		}

		public int BonusStrength { get; protected set; }

		public event BonusEatenEventHandler BonusEaten;

		public delegate void BonusEatenEventHandler();

		private bool _isBonusEaten;
		internal bool IsBonusEaten
		{
			get => _isBonusEaten;
			set
			{
				if (value)
				{
					BonusEaten?.Invoke();
				}
				_isBonusEaten = value;
			}
		}
	}
}