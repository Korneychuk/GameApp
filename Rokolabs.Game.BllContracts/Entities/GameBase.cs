﻿using System;

namespace Rokolabs.Game.BllContracts.Entities
{
	public abstract class GameBase
	{
        public Guid Id { get; }
        public string BaseType => GetType().Name;

        public GameBase()
        {
            Id = Guid.NewGuid();
        }
	}
}