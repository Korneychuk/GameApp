﻿using System.Collections.Generic;
using Rokolabs.Game.BllContracts.Entities.Structures;

namespace Rokolabs.Game.BllContracts.Entities.Field
{
	public class FieldPoints
	{
		private readonly FieldPoint[,] _fieldPoints;

		public FieldPoint GetByAddress(SPoint point)
		{
			return _fieldPoints[point.I, point.J];
		}

		public IEnumerable<FieldPoint> GetFieldPoints()
		{
			{
				for (int i = 0; i < _fieldPoints.GetLength(0); i++)
				{
					for (int j = 0; j < _fieldPoints.GetLength(1); j++)
					{
						yield return GetByAddress(new SPoint(i, j));
					}
				}
			}
		}

		public FieldPoints (int width, int hight)
		{
			_fieldPoints = new FieldPoint[width, hight];

			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < hight; j++)
				{
					_fieldPoints[i, j] = new FieldPoint(this, i, j);
				}
			}
		}
	}
}