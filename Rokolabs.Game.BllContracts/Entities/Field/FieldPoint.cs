﻿using System;

namespace Rokolabs.Game.BllContracts.Entities.Field
{
	public class FieldPoint
	{
		/*internal*/ public FieldPoint() { }

		internal FieldPoint(FieldPoints gamePoints, int i, int j)
		{
			GamePoints = gamePoints;
			I = i;
			J = j;
		}

		public GameBase GameBase { get; /*internal*/ set; }

		public Guid? BaseId { get; /*internal*/ set; }

		public int I { get; /*internal*/ set; }

		public int J { get; /*internal*/ set; }

		internal FieldPoints GamePoints { get; set; }
	}
}