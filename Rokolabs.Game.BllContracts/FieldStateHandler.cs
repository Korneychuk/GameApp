﻿using Rokolabs.Game.BllContracts.Entities;

namespace Rokolabs.Game.BllContracts
{
	public delegate void FieldStateHandler(GameState gameState);
}