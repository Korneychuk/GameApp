﻿using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Bonuses;
using Rokolabs.Game.BllContracts.Entities.Enemies;
using Rokolabs.Game.BllContracts.Entities.Field;
using Rokolabs.Game.Dto;
using System;

namespace Rokolabs.Game.BllContracts.Mapping
{
	public class Mapper
	{
		public static GameBaseDto DtoFromGameBase(GameBase gameBase)
		{
            if(gameBase is null)
            {
                return null;
            }

			return new GameBaseDto(gameBase.Id, gameBase.BaseType, (gameBase as Enemy)?.Damage, (gameBase as Hero)?.HealthPoints,
				(gameBase as Bonus)?.BonusStrength);
		}

		public static FieldPoint FieldPointFromDto(FieldPointDto fieldPointDto)
		{
			return new FieldPoint
			{
				BaseId = fieldPointDto.BaseId,
				GameBase = GameBaseFactory.GetGameBase(fieldPointDto.GameBaseDto),
				I = fieldPointDto.I,
				J = fieldPointDto.J
			};
		}

		public static FieldPointDto DtoFromFieldPoint(FieldPoint fieldPoint)
		{
            var gameBaseDto = DtoFromGameBase(fieldPoint.GameBase);

            return new FieldPointDto(fieldPoint.I, fieldPoint.J, gameBaseDto?.Id, gameBaseDto);
		}

		public static Record RecordFromDto(RecordDto recordDto)
		{
			return new Record(recordDto.PlayerName, recordDto.Score);
		}

		public static RecordDto DtoFromRecord(Record record)
		{
			return new RecordDto(record.PlayerName, record.Score);
		}
	}
}