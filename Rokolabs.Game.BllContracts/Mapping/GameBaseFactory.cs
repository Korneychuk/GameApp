﻿using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Bonuses;
using Rokolabs.Game.BllContracts.Entities.Enemies;
using Rokolabs.Game.BllContracts.Entities.Hurdles;
using Rokolabs.Game.Dto;
using Rokolabs.Game.Dto.Enums;

namespace Rokolabs.Game.BllContracts.Mapping
{
	public class GameBaseFactory
	{
		public static GameBase GetGameBase(GameBaseDto gameBaseMapper)
		{
            if (gameBaseMapper is null)
                return null;

			switch (gameBaseMapper.BaseType)
			{
				case BaseType.Hero:
					return new Hero(gameBaseMapper.HealthPoints);

				case BaseType.AppleBonus:
					return new AppleBonus(gameBaseMapper.BonusStrength);

				case BaseType.RaspberryBonus:
					return new RaspberryBonus(gameBaseMapper.BonusStrength);

				case BaseType.BearEnemy:
					return new BearEnemy(gameBaseMapper.Damage);

				case BaseType.WolfEnemy:
					return new BearEnemy(gameBaseMapper.Damage);

				case BaseType.RockHurdle:
					return new RockHurdle();

				case BaseType.TreeHurdle:
					return new TreeHurdle();

				default: return null;
			}
		}
	}
}