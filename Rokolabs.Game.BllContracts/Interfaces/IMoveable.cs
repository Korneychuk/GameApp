﻿using Rokolabs.Game.BllContracts.Entities.Field;

namespace Rokolabs.Game.BllContracts.Interfaces
{
	public interface IMoveable
	{
		FieldPoint Move(FieldPoint fieldPoint, FieldPoint oldFieldPoint);
	}
}