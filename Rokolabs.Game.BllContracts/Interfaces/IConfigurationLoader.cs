﻿using System.Collections.Generic;
using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Field;

namespace Rokolabs.Game.BllContracts.Interfaces
{
	public interface IConfigurationLoader
	{
		List<FieldPoint> LoadConfiguration();

		void UpdateConfiguration(IEnumerable<FieldPoint> fieldPoints);

		void UpdateRecordsTable(Record record);

		List<Record> GetRecordsTable(int count);
	}
}