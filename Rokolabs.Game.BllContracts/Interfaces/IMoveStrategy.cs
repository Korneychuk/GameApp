﻿using Rokolabs.Game.Dto.Enums;

namespace Rokolabs.Game.BllContracts.Interfaces
{
	public interface IMoveStrategy
	{
		MoveDirection CalculateNextMoveDirection();
	}
}