﻿using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.Dto.Enums;

namespace Rokolabs.Game.BllContracts.Interfaces
{
	public interface IGameController
	{
		event FieldStateHandler StateChanged;

		GameState GetCurrentGameState();

		GameStatus GetGameStatus();

		void Start();

		void MoveHero(MoveDirection moveDirection);

		void Stop();

		void Reset();
	}
}