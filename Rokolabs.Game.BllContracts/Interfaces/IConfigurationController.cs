﻿using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.BllContracts.Entities.Structures;

namespace Rokolabs.Game.BllContracts.Interfaces
{
	public interface IConfigurationController
	{
		void InitializeConfig(int fieldWidth, int fieldHight);

		void AddOrUpdateGameEntity(GameBase gameBase, SPoint position);

		void RemoveGameEntity(SPoint position);

		bool SaveConfiguration();

		void ClearGameField();
	}
}