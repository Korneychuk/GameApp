﻿using Rokolabs.Game.BllContracts.Entities;
using Rokolabs.Game.Dto.Enums;

namespace Rokolabs.Game.BllContracts.Interfaces
{
	public interface IFieldController
	{
		event FieldStateHandler FieldChanged;

		void Iterate(MoveDirection moveDirection);

		void SetGameField(int fieldWidth, int fieldHight);

		GameState GetCurrentGameState();

		void Initialize();
	}
}