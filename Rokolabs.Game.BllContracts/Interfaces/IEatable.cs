﻿using Rokolabs.Game.BllContracts.Entities.Bonuses;

namespace Rokolabs.Game.BllContracts.Interfaces
{
	public interface IEatable
	{
		void EatBonus(Bonus bonus);
	}
}