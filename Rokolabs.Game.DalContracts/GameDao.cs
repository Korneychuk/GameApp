﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Rokolabs.Game.Dto;
using Rokolabs.Game.SqlDal;

namespace Rokolabs.Game.DalContracts
{
	public class GameDao : IGameDao
	{
		private readonly string _connectionString;

		public GameDao()
		{
			_connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["GameStorage"].ConnectionString;
		}
	
		public IEnumerable<FieldPointDto> LoadConfiguration()
		{
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				var gameField = connection.Query<FieldPointDto>("GetFieldPoints", commandType: CommandType.StoredProcedure);

				foreach (var fieldPoint in gameField)
				{
					if (fieldPoint.BaseId != null)
					{
						var gameBase = connection.QueryFirstOrDefault<GameBaseDto>(
							"GetBaseByID", new {fieldPoint.BaseId}, commandType: CommandType.StoredProcedure);
						fieldPoint.GameBaseDto = gameBase;
						yield return fieldPoint;
					}
				}
			}
		}

		public void UpdateConfiguration(IEnumerable<FieldPointDto> fieldPoints)
		{
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				foreach (var fieldPoint in fieldPoints)
				{
					if (fieldPoint.GameBaseDto != null)
					{
						connection.Execute("AddGameBase", fieldPoint.GameBaseDto, commandType: CommandType.StoredProcedure);
					}

					connection.Execute("AddFieldPoint", new {fieldPoint.I, fieldPoint.J, fieldPoint.BaseId},
						commandType: CommandType.StoredProcedure);
				}
			}
		}

		public void UpdateRecordsTable(RecordDto record)
		{
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				connection.Execute("AddRecord", record, commandType: CommandType.StoredProcedure);
			}
		}

		public IEnumerable<RecordDto> GetRecordsTable(int count)
		{
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				return connection.Query<RecordDto>("GetRecords", new {count}, commandType: CommandType.StoredProcedure);
			}
		}
	}
}